import UIKit

extension UITableView {
    func hideEmptyCells() {
        tableFooterView = UIView(frame: .zero)
    }
}
